//
//  File.swift
//  
//
//  Created by Владимир Никитин on 21.05.2023.
//

import Foundation
import Fluent
import Vapor

final class Train: Model, Content{

    
    
    static var schema: String = "trains"
    
    @ID
    var id : UUID?
    
    @Field(key: "title") var title: String
    @Field(key: "description") var description: String
    @Field(key: "slug") var slug: String
    @Field(key: "category") var category: String
    @Field(key: "image") var image: String
    
    init(){}
    
    init(id: UUID? = nil, title: String, description: String, slug: String, category: String, image: String) {
        self.id = id
        self.title = title
        self.description = description
        self.slug = slug
        self.category = category
        self.image = image
    }
}
