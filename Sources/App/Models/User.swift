//
//  File.swift
//  
//
//  Created by Владимир Никитин on 07.06.2023.
//

import Vapor
import Fluent

final class User: Model, Content{
    
    static var schema: String = "users"
    
    @ID var id: UUID?
    @Field(key: "name") var name: String
    @Field(key: "login") var login: String
    @Field(key: "password") var password: String
    @Field(key: "status") var status: String
    @Field(key: "profilePic") var profilePic: String
  
    final class Public : Content{
        var id: UUID?
        var login: String
        var status: String
        var profilePic: String?
        
        init(id: UUID? = nil, login: String, status: String, profilePic: String? = nil) {
            self.id = id
            self.login = login
            self.status = status
            self.profilePic = profilePic
        }
    }
}

extension User: ModelAuthenticatable{
    static let usernameKey = \User.$login
    
    static var passwordHashKey = \User.$password
    
    func verify(password: String) throws -> Bool {
        try Bcrypt.verify(password, created: self.password)
    }
    
}

extension User{
    func converToPublic()-> User.Public{
        let pub = User.Public(id: self.id, login: self.login, status: self.status, profilePic: self.profilePic)
        return pub
    }
}

enum AccountStatus: String{
    case free = "Бесплатный"
    case pro = "Платный"
}
