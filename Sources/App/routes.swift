import Fluent
import Vapor

func routes(_ app: Application) throws {
  

    try app.register(collection: TrainsController())
    try app.register(collection: UsersController())

    app.routes.defaultMaxBodySize = "5Mb"
    
}
