//
//  File.swift
//  
//
//  Created by Владимир Никитин on 21.05.2023.
//

import Foundation
import Fluent
import Vapor

struct CreateTrain: AsyncMigration{
    func prepare(on database: FluentKit.Database) async throws {
        
        let schema = database.schema("trains")
        
            .id()
            .field("title", .string, .required)
            .field("description", .string, .required)
            .field("slug", .string, .required)
            .field("category", .string, .required)
            .field("image", .string, .required)
            
        try await schema.create()
    }
    
    func revert(on database: FluentKit.Database) async throws {
        try await database.schema("trains").delete()
    }
    
    
}
