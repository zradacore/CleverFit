//
//  File.swift
//  
//
//  Created by Владимир Никитин on 07.06.2023.
//

import Vapor
import Fluent

struct UsersController: RouteCollection{
    func boot(routes: RoutesBuilder) throws {
        let userGroup = routes.grouped("users")
        userGroup.post(use: createHandler)
        userGroup.get(use: getAllHandler)
        userGroup.get(":id", use: getHandler)
        userGroup.post("auth", use: authHandler)
    }
    
    func createHandler(_ req : Request) async throws -> User.Public{
        let user = try req.content.decode(User.self)
        
        user.password = try Bcrypt.hash(user.password)
        do{
            try await user.save(on: req.db)
        }catch{
            String(reflecting: error)
        }
        return user.converToPublic()
    }
    
    func getAllHandler(_ req: Request) async throws -> [User.Public]{
        let user = try await User.query(on: req.db).all()
        let publics = user.map { user in
            user.converToPublic()
        }
        return publics
    }
    
    
    func getHandler(_ req: Request) async throws -> User.Public{
        guard let user = try await User.find(req.parameters.get("id"), on: req.db) else{
            throw Abort(.notFound)
        }
        
        return user.converToPublic()
    }
    
    
    func authHandler(_ req: Request) async throws -> User.Public{
        let userDTO = try req.content.decode(AuthUserDTO.self)
        guard let user = try await User.query(on: req.db).filter("login", .equal, userDTO.login)
            .first() else {
            throw Abort(.notFound)
        }
        
        let isPassEqual = try Bcrypt.verify(userDTO.password, created: user.password)
        guard isPassEqual else { throw Abort(.unauthorized)}
        return user.converToPublic()
    }
    
}


struct AuthUserDTO: Content{
    let login: String
    var password: String
}
