//
//  TrainsController.swift
//  
//
//  Created by Владимир Никитин on 30.05.2023.
//

import Foundation
import Fluent
import Vapor

struct TrainsController: RouteCollection{
    
    func boot(routes: Vapor.RoutesBuilder) throws {
        let trainsGroup = routes.grouped("trains")
        
        trainsGroup.get(use: getAllHandler)
        trainsGroup.get(":trainID", use: getHandler)
        
        let basicMW = User.authenticator()
        let guardMW = User.guardMiddleware()
        let protected = trainsGroup.grouped(basicMW, guardMW)
        
        protected.post(use: createHandler)
        
        protected.delete(":trainID", use: deleteHandler)
        protected.put(":trainID", use: updateHandler)
    }
    
    func createHandler(_ req: Request) async throws -> Train{
        
        guard let trainData = try? req.content.decode(TrainDTO.self) else {
            throw Abort(.custom(code: 499, reasonPhrase: "Не получилось декодировать контент в модель тренировки"))
        }
        
        let train = Train(id: trainData.id,title: trainData.title, description: trainData.description, slug: trainData.slug, category: trainData.category, image: "")
        
        let imagePath = req.application.directory.workingDirectory + "/Storage/Trains" + "/\(train.id).jpg"
        
        try await req.fileio.writeFile(.init(data: trainData.image), at: imagePath)
        
        train.image = imagePath
        
        try await train.save(on: req.db)
        return train
    }
    
    func getAllHandler(_ req: Request) async throws -> [Train] {
        let trains = try await Train.query(on: req.db).all()
        return trains
    }
    
    func getHandler(_ req: Request) async throws -> Train{
        
    
        guard let train = try await Train.find(req.parameters.get("trainID"), on: req.db) else {
            

            
            throw Abort(.notFound)
        }
        return train
         
    }
    
    
    func updateHandler(_ req: Request) async throws -> Train{
        
        
        
        guard let train = try await Train.find(req.parameters.get("trainID"), on: req.db) else {
            
            throw Abort(.notFound)
        }
        
        let updatedProduct = try req.content.decode(Train.self)
        train.title = updatedProduct.title
        train.category = updatedProduct.category
        train.image = updatedProduct.image
        train.description = updatedProduct.description
        train.slug = updatedProduct.slug
        try await train.save(on: req.db)
        
        return train
    }
    
    
    
    func deleteHandler(_ req: Request) async throws -> HTTPStatus{
        print(req.parameters.allNames)
        guard let train = try await Train.find(req.parameters.get("trainID"), on: req.db) else{
            throw Abort(.notFound)
        }
        try await train.delete(on: req.db)
        
        return .ok
        
    }
}


//Domen transfer object
struct TrainDTO: Content{
    var id: UUID
    var title: String
    var description: String
    var slug: String
    var category: String
    var image: Data
    
}
